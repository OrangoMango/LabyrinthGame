# LabyrinthGame
[TO BE CONTINUED] Easy java game (Feb 2021)

# Features
* Play a labyrinth 
* Create a level with the editor (Many block types available and obstacles to avoid)
* **Beta:** Engineering mode, create your own electric circuits to add action on your level
* *(Other features will be added very soon)*

# Run program
## Requirements
* Java JDK version 11+
* JavaFX 11+ (Download link: https://gluonhq.com/products/javafx)
* Windows/Linux/MacOS operating system
## How to do
* Change the first line of the `run.sh` file according to your javafx path location
* Execute the `run.sh` file in the `app/` folder with the command:
```bash
sh run.sh
```

# Screenshots
* Gameplay (*Use arrows to move*)<br>
![slvl8](https://user-images.githubusercontent.com/61402409/133892713-7ceba2d7-258f-4487-8f28-ba2a4403cf81.png)
![slvl6](https://user-images.githubusercontent.com/61402409/133892711-703bad4a-b670-46ee-86e9-e811b87e280d.png)
* Editor (*Click on the block to change its type*)<br>
![editor3_4](https://user-images.githubusercontent.com/61402409/133609868-f92db532-ac04-4317-a8bd-41371f69a173.png)
* `My levels` interface (*Edit, delete and run levels*)<br>
![labgame](https://user-images.githubusercontent.com/61402409/121567737-37915380-ca1f-11eb-8de0-25b3fea0f7ad.png)
